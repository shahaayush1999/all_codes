#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double pow3(int a, int b);

int main() {
	int x = 0, y = 0;
	scanf("%d%d", &x, &y);
	printf("%lf\n", pow3(x, y));
	return 0;
}

double pow3(int base, int pow) {
	int res = 1, c = 0, sign = 1;
	
	if (pow < 0)
		sign = -1;
	pow *= sign;
		
	if (base == 0) {
		if (pow == 0)
			return NAN;
		else
			return 0;
	}
	
	else if (pow == 0)
		return 1;
	
	while(pow >= 1) {
		c = pow % 3;
		if (c) {
			while (c > 0) {
				res = res * base;
				--c;
			}
		}
		pow = pow / 3;
		if (pow < 1)
			break;
		base = base * base * base;
	}
	if (sign == -1)
		return 1 / (float)res;
	return res;
}
