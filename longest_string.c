#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
	int n = 0, x = 1;
	char *ln, *temp;
	ln = malloc(100 * sizeof(char));
	temp = malloc(100 * sizeof(char));

	do {
		printf("S %d: ", n + 1);
		fgets(temp,99,stdin);
		printf("string: %s", temp);
		while(x) {
			strcpy(ln, temp);
			--x;
		}
		if(strcmp(temp, "-1\n") == 0)
			break;
		if (strlen(temp) > strlen(ln))
			strcpy(ln, temp);
		++n;
	}
	while (1);
	printf("The longest string = %s", ln);
	return 0;
}
