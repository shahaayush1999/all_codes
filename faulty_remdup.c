#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define PREC 0.0000001 /*PREC = precision for float comparison*/

int equal(double *a, double *b); /*float comparison*/
int removeduplicate(double a[], int indx);
int readarray(double *arr, int *indx, int blocksize); /*Dynamic size allocation*/


int main() {
	int blocksize = 1, x;
	int indx = 0; /*Index to maintain universal value for digits entered*/
	double *arr = NULL;
	if (readarray(arr, &indx, blocksize) == 1) /*first execute readarray, then Check if failure*/
		return 1;
	for (x = 0; x < indx; x++) {
		printf("%lf\n", arr[x]);
	}
	printf("All working\n");
	indx = removeduplicate(arr, indx);
	for (x = 0; x < indx; x++) {
		printf("%lf\n", arr[x]);
	}
	return 0;
}


/*Returns 0 if working and 1 if fail*/
int readarray(double *arr, int *indx, int blocksize){
	printf("Going for realloc %d:\n", blocksize);
	arr = (double *)realloc(arr, sizeof(double) * (4 * blocksize));
	if (arr == NULL) /*Check if failure*/
		return 1;
	while ((printf("input %d: ", *indx) != 0) && (scanf("%lf", &arr[*indx]) == 1)) {
		if ((*indx) == ((4 * blocksize) - 1)) {  /*Check if indx will cross 4 size of alloced block. if yes, realloc to increase size*/
			++blocksize;
			++(*indx);
			printf("Transfer to realloc %d with index %d\n", blocksize, *indx);
			if (readarray(arr, indx, blocksize) == 1) /*first execute readarray, then Check if failure*/
				return 1;
			return 0;
		}
		++(*indx);
	}
	if (feof(stdin)) {
		printf("\nfeof successful\n");
		return 0;
	}
		return 1; /*If returning without reaching EOF, means an error*/
}


int removeduplicate(double a[], int indx) {
	int changes = 0, x, y, z;
	printf("Remove Duplicate index: %d\n", indx);
	for(x = 0; x < (indx - 1 - changes); x++)
		for(y = (x + 1); y < indx; y++){
			printf("Comparing a[%d] and a[%d]\n", x, y);
			sleep(1);
			if (equal(&a[x], &a[y]))
				for(z = y; z < (indx - 1); z++) {
					a[z] = a[z + 1];
					++changes;
				}
		}
	printf("Changes made: %d\n", changes);
	return (indx - changes);
}

int equal(double *a, double *b) {
		if ((*b > (*a - (double)PREC)) && (*b < (*a + (double)PREC))) {
			sleep(1);
			return 1;
		}
		else
			return 0;
		return 0;
}

