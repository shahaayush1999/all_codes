#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

void swap(int *xp, int *yp);
void bubbleSort(int arr[], int n);
void printArray(int arr[], int size);

int main() {
	int *arr, count = 0, num, size = 1024;
	struct timeval tv1, tv2;
	double tm;
	arr = (int *)malloc(sizeof(int) * size);
	if(arr == NULL) {
		printf("Failed for %d\n", size);
		exit(1);
	}
	while(scanf("%d", &num) != EOF) {
		arr[count] = num;
		count++;
		if(count == size) {
			size *= 2;
			arr = (int *)realloc(arr, sizeof(int) * size);
			if(arr == NULL) {
				printf("Failed for %d\n", size);
				exit(1);
			}
		}
	}
	gettimeofday(&tv1, NULL);
	bubbleSort(arr, count);
	gettimeofday(&tv2, NULL);
	//printArray(arr, count);
 	tm = (tv2.tv_sec - tv1.tv_sec) + (tv2.tv_usec - tv1.tv_usec)/1000000.0;
 	printf("%lf\n", tm);
	free(arr);
	return 0;
}

void swap(int *xp, int *yp) {
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

/* A function to implement bubble sort */
void bubbleSort(int arr[], int n) {
	int i, j;
	for (i = 0; i < n - 1; i++) /*Last i elements are already in place*/
		for (j = 0; j < n - i - 1; j++)
			if (arr[j] > arr[j + 1])
				swap(&arr[j], &arr[j + 1]);
}

/* Function to print an array */
void printArray(int arr[], int size) {
	int i;
	for (i = 0; i < size; i++)
		printf("%d\n", arr[i]);
}
