#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


typedef struct data {
	int age;
	char name[16];
}data;

int main(int argc, char *argv[]) {
	char tempchar;
	data tempdata;
	int n = 0, m = 0, p = 0, tempint = 0;
	if(argc != 2) {
		errno = EINVAL;
		perror("ERROR:");
		return errno;
	}	
	
	int fp = open(argv[1], O_RDONLY);
	if(fp == -1) {
		perror("ERROR");
		return errno;
	}
	
	if(read(fp, &n, sizeof(int))) {
		while(n) {
			read(fp, &tempint, sizeof(int));
			printf("%d\n", tempint);
			--n;
		}
	}
	if(read(fp, &m, sizeof(int))) {
		while(m) {
			read(fp, &tempdata, sizeof(data));
			printf("%d %s\n", tempdata.age, tempdata.name);
			--m;
		}
	}
	if(read(fp, &p, sizeof(int))) {
		while(p) {
			while(1) {
				read(fp, &tempchar, sizeof(char));
				if(tempchar != '\0')
					printf("%c", tempchar);
				else
					break;
			}
			printf("\n");
			--p;
		}
	}
	close(fp);
	return 0;
}
