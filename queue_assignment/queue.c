#include "queue.h"
#include <stdlib.h>

/*
typedef struct data {
	char name[16];
	unsigned int age;
} data;

typedef struct node {
	data d;
	struct node *next;
} node;

typedef struct queue {
	node *head;
	node *tail;
} queue;
*/
void qinit(queue *q) {
	q->head = NULL;
	q->tail = NULL;
}
void enq(queue *q, data a) {
	node *temp = q->tail;
	q->tail = (node *)malloc(sizeof(node));
	(q->tail)->d = a;
	if(q->head == NULL) {
		q->head = q->tail;
	}
	else {
		temp->next = q->tail;
	}
	q->tail->next = q->head;
}
data deq(queue *q) {
	data a;
	if(q->head == q->tail) {
		a = q->head->d;
		q->head = NULL;
		q->tail = NULL;
	}
	else {
		q->tail->next = q->head->next;
		a = q->head->d;
		q->head = q->head->next;
	}
	return a;
}
int qfull(queue *q) {
	return 0;
}
int qempty(queue *q) {
	if(!q->head)
		return 1;
	return 0;
}
