#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


typedef struct info { 
	int id;
	char name[16];
	double score;
} info;

int main(int argc, char *argv[]) {
	if (argc != 2) {
		errno = EINVAL;
		perror("ERROR:");
		return errno;
	}
	/*write mode*/
	int fp = open(argv[1], O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR);
	if(fp == -1) {
		perror("ERROR");
		return errno;
	}
	info infoarr;
	int count = 0, id;
	char name[16];
	double score;
	
	while(scanf("%d%s%lf", &id, name, &score) == 3) {
		infoarr.id = id;
		strcpy(infoarr.name, name);
		infoarr.score = score;
		write(fp, &infoarr,sizeof(info));
		count++;
	}
	close(fp);
	return 0;
}
