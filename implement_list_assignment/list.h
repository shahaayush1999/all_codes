//create a singly linked null terminated list
typedef struct node {
	char *string;
	struct node *next;
} node;

typedef struct list {
	node *head;
	int count;
} list;

void init(list *l);
void insert(list *l, char *str, int pos);
char *remov(list *l, int pos);
void sort(list *l);
void reverse(list *l);
