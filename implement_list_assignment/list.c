#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "list.h"
void init(list *l) {
	l->head = NULL;
	l->count = 0;
}

int listerr() {
	fprintf(stderr, "Invalid node insertion in list");
	return INT_MIN;
}

void insert(list *l, char *str, int pos) {
	if(pos < 0 || pos > l->count ) {
		fprintf(stderr, "Invalid node insertion in list\n");
		return;
	}
	node *tmp = (l->head), *tmpnext;
	int strlength = strlen(str), flag = 0;
	if(pos == l->count) {
		flag = 1;
	}
	while(pos > 0) {
		(tmp) = (tmp)->next;
		pos--;
	}
	if(flag) {
		tmpnext = NULL;
	}
	else
		tmpnext = (tmp)->next;
		
	(tmp) = (node *)malloc(sizeof(node));
	(tmp)->next = tmpnext;
	(tmp)->string = (char *)malloc((strlength + 1) * sizeof(char));
	strcpy((tmp)->string, str);
	(l->count)++;
}

char *remov(list *l, int pos) {
	node **tmp;
	
}

void sort(list *l) {

}

void reverse(list *l){

}

void printlist(list l) {
	node *trav = l.head;
	while(trav) {
		printf("%s ", trav->string);
		trav = trav->next;
	}
	printf("\n");
}

int main() {
	list l;
	init(&l);
	char arr[50];
	//char *rem;
	int position = 0;
	while(scanf("%s ", arr) != -1) {
		insert(&l, arr, position);
		++position;
	}
	printlist(l);
	//rem = remov(&l, 2);
	//printlist(l);
	//reverse(&l);
	//printlist(l);
	//sort(&l);
	//printlist(l);
	return 0;
}
