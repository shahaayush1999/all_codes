#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define PREC 0.000001

typedef struct info {
	int id;
	char name[16];
	double score;
}info;

int equal(double a, double b);

int main(int argc, char *argv[]) {
	double scorecheck;
	int count = 0;
	info infoarr;
	if(argc != 3) {
		errno = EINVAL;
		perror("ERROR");
		return errno;
	}
	scorecheck = atof(argv[2]);
	int fp = open(argv[1], O_RDONLY);
	if(fp == -1) {
		perror("ERROR");
		return errno;
	}
	while(read(fp, &infoarr, sizeof(info)) == sizeof(info)) {
		if(equal(infoarr.score, scorecheck)) {
			count++;
			printf("%d %s %lf\n", infoarr.id, infoarr.name, infoarr.score);
		}
	}
	if(count == 0)
		printf("No match found\n");
	close(fp);
	return 0;
}

int equal(double a, double b) {
	if((b < (a + PREC)) && (b > (a - PREC)))
		return 1;
	return 0;
}
