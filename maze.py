def print_maze(maze):
	for i in maze:
		print ' '.join(map(str, i))

def is_valid(arr, x, y, n, visited):
	if x >= 0 and y >= 0 and x < n and y < n and arr[x][y] != 1:
		if (x, y) not in visited:
			return 1
	return 0

def solve_maze(arr, n):
	solution = [[0 for i in range(n)] for j in range(n)]
	visited = set()
	stack = []

	start = (0, 0) #start point
	current = start #To initialise

	visited.add(current) #Remember that we already visited current

	while current != (n - 1, n - 1): #Stop when we are at the end
		x_curr = current[0]
		y_curr = current[1]
		solution[x_curr][y_curr] = 1

		if is_valid(arr, x_curr + 1, y_curr, n, visited): #moving right
			visited.add((x_curr + 1, y_curr))
			stack.append(current)
			current = (x_curr + 1, y_curr)
		elif is_valid(arr, x_curr - 1, y_curr, n, visited): #moving left
                        visited.add((x_curr - 1, y_curr))
			stack.append(current)
                        current = (x_curr - 1, y_curr)
		elif is_valid(arr, x_curr, y_curr + 1, n, visited): #moving down
			visited.add((x_curr, y_curr + 1))
			stack.append(current)
			current = (x_curr, y_curr + 1)
		elif is_valid(arr, x_curr, y_curr - 1, n, visited): #moving up
                        visited.add((x_curr, y_curr - 1))
			stack.append(current)
                        current = (x_curr, y_curr - 1)
		else:
			try:
				current = stack.pop()			
			except:
				return None
			solution[x_curr][y_curr] = 0
	solution[n - 1][n - 1] = 1
	return solution #When we do reach the end

N = int(raw_input())
maze = [[0 for i in range(N)] for j in range(N)]
for i in range(N):
	maze[i] = map(int, raw_input().strip().split())
"""
N = 5
maze = [ [0, 1, 1, 1, 1],
 	 [0, 1, 0, 1, 0],
 	 [0, 1, 0, 0, 0],
	 [0, 0, 0, 1, 0],
	 [1, 1, 1, 1, 0]
]
print
"""
print_maze(solve_maze(maze, N))

