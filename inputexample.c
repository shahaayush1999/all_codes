#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
int main() {
	long long *a, i, n, count = 0, num, size = 1024, sum; 
	struct timeval tv1, tv2;
	a = (long long *)malloc(sizeof(long long) * size);
	/* Do "man scanf". Scanf returns -1 when there is "no input" 
	 * To tell scanf that there is no input, you can press "control-d" 
	 * in stdio.h EOF is #defined to be -1 */
	while(scanf("%lld", &num) != EOF) {
		a[count] = num;
		count++;
		if(count == size) {
			size *= 2;
			a = (long long *)realloc(a, sizeof(long long) * size);
			if(a == NULL) {
				printf("Failed for %lld\n", size);
				exit(1);
			}
		}
	}
	//t1 = getimeofday();	
	for(i = 0; i < count; i++) 
		sum = sum + a[i];
	//t2 = getimeofday();	
	
	printf("%d\n", sum);
	free(a);
	return 0;
}
