#include <stdio.h>
#include <stdlib.h>

int main() {
	int *count, x = 0, i = 0;
	int c = 0;

	count = (int *) calloc(256, sizeof(int));
	while ((c = getchar()) != EOF)
		++count[(int)c];
	for (x = 0; x < 256; x++) {
		printf("Out %d: ",x);
		for(i = 0; i < count[x]; i++)
			printf("|");
		printf("\n");
	}
	return 0;
}
