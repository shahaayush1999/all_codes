#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int readline (int fp, char *a);

typedef struct score {
	char name[30];
	int mis;
	int marks;
} score;

int main(int argc, char *argv[]) {
	if(argc != 3) {
		errno = EINVAL;
		perror("ERROR:");
		return errno;
	}
	score list;
	char line[1024], *token, *ptr;
	ptr = &line[0];
	token = (char *)malloc(30);
	int marks = atoi(argv[2]);
	int fp = open(argv[1], O_RDONLY), x;
	if(fp == -1) {
		perror("ERROR");
		return errno;
	}
	while(1) {
		x = readline(fp, line);
		if(x == 1)
			break;
		token = &line[0];
		token = strtok(ptr, ", ");
		if(token == NULL)
			break;
		strcpy(list.name, token);
		token = strtok(NULL, ", ");
		if(token == NULL)
			break;
		list.mis = atoi(token);
		token = strtok(NULL, ", ");
		if(token == NULL)
			break;
		list.marks = atoi(token);
		if(list.marks == marks)
			printf("%s %d %d\n", list.name, list.mis, list.marks);
	}
	
	close(fp);
	return 0;
}

int readline (int fp, char *a) {
	int x;
	char c;
	while((x = read(fp, &c, 1))) {
		if(c == '\n')
			break;
		*a = c;
		a++;
	}
	*a = '\0';
	if (x == 0)
		return 1;
	return 0;
}
