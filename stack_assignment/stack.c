#include <stdio.h>
#include <stdlib.h>
typedef struct stack {
	int *x;
	int size;
} stack;

void init(stack *s) {
	s->x = (int *) malloc(sizeof(int) * 512);
	s->size = 0;
}

int isempty(stack *s) {
	return s->size == 0;
}

int isfull(stack *s) {
	return s->size == 512;
}

void push(stack *s, int x) {
	s->x[s->size] = x;
	(s->size)++;
}

int pop(stack *s) {
	(s->size)--;
	return s->x[s->size];
}


int main() {
	stack s;
	int x;
	init(&s);
	
	while(scanf("%d", &x) != -1)
		if(!isfull(&s))
			push(&s, x);

	while(!isempty(&s))
		printf("%d\n", pop(&s));

	return 0;
}
