from random import randint
print("WELCOME TO GUESS ME!")
print("I'm thinking of a number between 1 and 100")
print("If your guess is more than 10 away from my number, I'll tell you you're COLD")
print("If your guess is within 10 of my number, I'll tell you you're WARM")
print("If your guess is farther than your most recent guess, I'll say you're getting COLDER")
print("If your guess is closer than your most recent guess, I'll say you're getting WARMER")
print("LET'S PLAY!")
count = 1
num = randint(1,100)
guess = int(input('Guess:'))
diff = abs(num - guess)
if diff == 0:
    print('RIGHT GUESS')
elif diff > 10:
    print('COLD')
elif diff < 10:
    print('HOT')
count+=1
while True:
    guess = int(input('Guess:'))
    temp = abs(num - guess)
    if temp == 0:
        print('RIGHT GUESS')
        break
    elif diff > temp:
        print('WARMER')
    elif diff < temp:
        print('COLDER')
    elif diff == temp:
        print('I FEEL NOTHING')
    diff = temp
    count+=1
print('You took ' + str(count) + ' tries to guess right answer')
