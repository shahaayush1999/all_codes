#include <stdio.h>

#define PREC 0.000000001

int removeduplicate(double a[], int n);
int equal(double *a, double *b);

int main() {
	double a[512];
	int indx = 0, x = 0, n;
	while (printf("indx %d: ", indx) && scanf("%lf", &a[indx]) == 1) {
		if(indx == 511) {
			printf("\n");
			break;
		}
		++indx;
	}
	n = removeduplicate(a, indx);
	for (x = 0; x < n; x++)
		printf("%lf\n",a[x]);
	return 0;	
}

int equal(double *a, double *b) {
	if ((*b > (*a - (double)PREC)) && (*b < (*a + (double)PREC)))
		return 1;
	else
		return 0;
	return 0;
}

int removeduplicate(double a[], int n) {
	int x, y, z, changes = 0;
	for(x = 0; x < n - 1 - changes; x++) {
		for(y = x + 1; y < n - changes; y++) {
			if(a[x] == a[y]) {
				for(z = y; z < n - 1 - changes; z++) {
					a[z] = a[z + 1];
				}
				++changes;
				y = x;
			}
		}
	}
	return (n - changes);
}
