#include <stdio.h>
#include <stdlib.h>
#include "integer.h"
#define MAX 128


/* Initialize the "Integer" type data structure.
 * You may interpret an "empty" Integer to be the value 0.
 */
void initInteger(Integer *a) {
	a->head = NULL;
	a->tail = NULL;
}


/* Appends one digit to an existing Integer data type.
 * E.g. if the Integer a was 1234 and the function was called as
 * addDigit(Integer *a, char c); where c='5'
 * then Integer a becomes 12345.
 * Ignore the request if c is not a digit.
 */
void addDigit(Integer *a, char c) {
	node *tmptail = a->tail;
	a->tail = (node *)malloc(sizeof(node));
	if(!a->head) {
		a->head = a->tail;
	}
	a->tail->next = NULL;
	a->tail->prev = tmptail;
	if(tmptail) {
		tmptail->next = a->tail;
	}
	a->tail->val = (unsigned int)c - '0';
}


void revaddDigit(Integer *a, char c) {
	node *tmphead = a->head;
	a->head = (node *)malloc(sizeof(node));
	if(!a->tail) {
		a->tail = a->head;
	}
	a->head->prev = NULL;
	a->head->next = tmphead;
	if(tmphead) {
		tmphead->prev = a->head;
	}
	a->head->val = (unsigned int)c - '0';
}


/* Create and return an Integer from a string.
 * 'str' may contain non-digits, in that case you should
 * process 'str' upto the first non-digit. E.g. if
 * 'str' was  123ab45 then the Integer returned will be 123
 * if 'str' was "abhijit" then the Integer returned will be 0
 */
Integer createIntegerFromString(char *str) {
	Integer a;
	initInteger(&a);
	int flag = 1;
	while(*str != '\0' && *str >= '0' && *str <= '9') {
		if(flag) {
			flag = (*str == '0'); //keep eating 0 till first non zero is encountered
			if(!flag)
				str--;
			str++;
		}
		else
			addDigit(&a, *str++);
	}
	return a;
}

/* This function adds two Integers a and b, and returns the sum as Integer */
Integer addIntegers(Integer a, Integer b) {
	Integer sum;
	initInteger(&sum);
	int carry = 0, x = 0, y = 0, z = 0;
	char c;
	node *ptra, *ptrb;
	ptra = a.tail;
	ptrb = b.tail;
	while(ptra || ptrb) {
		if(ptra == NULL)
			x = 0;
		else
			x = ptra->val;

		if(ptrb == NULL)
			y = 0;
		else
			y = ptrb->val;

		z = x + y + carry;
		printf("x = %d, y = %d, carry = %d, x = %d\n", x, y, carry, z);
		if(z > 9) {
			carry = 1;
			c = (char)(z % 10) + '0';
			revaddDigit(&sum, c);
			printf("revadddigit = %c\n", c);
		}
		else {
			carry  = 0;
			c = (char)z + '0';
			revaddDigit(&sum, c);
			printf("revadddigit = %c\n", c);
		}
		if(ptra != NULL)
			ptra = ptra->prev;
		if(ptrb != NULL)
			ptrb = ptrb->prev;
	}
	if(carry == 1) {
		c = (char)carry + '0';
		revaddDigit(&sum, c);
	}
	return sum;
}

/* This function substracts two Integers a and b, and returns the difference as Integer. Here a must be bigger than b (as Integer is non-zero). If a is smaller than b, then you can return an Integer with value 0 */
Integer substractIntegers(Integer a, Integer b) {
	Integer diff;
	initInteger(&diff);
	int borrow = 0, x = 0, y = 0, z = 0;
	char c;
	node *ptra, *ptrb;
	ptra = a.tail;
	ptrb = b.tail;
	while(ptra || ptrb) {
		if(ptra == NULL)
			x = 0;
		else
			x = ptra->val;

		if(ptrb == NULL)
			y = 0;
		else
			y = ptrb->val;

		z = x - y - borrow;
		printf("x = %d, y = %d, borrow = %d, x = %d\n", x, y, borrow, z);
		if(z > 9) {
			borrow = 1;
			c = (char)(z % 10) + '0';
			revaddDigit(&diff, c);
			printf("revadddigit = %c\n", c);
		}
		else {
			borrow  = 0;
			c = (char)z + '0';
			revaddDigit(&diff, c);
			printf("revadddigit = %c\n", c);
		}
		if(ptra != NULL)
			ptra = ptra->prev;
		if(ptrb != NULL)
			ptrb = ptrb->prev;
	}
	if(borrow == 1) {
		c = (char)borrow + '0';
		revaddDigit(&diff, c);
	}
	return diff;
}


/* Print an Integer (just the sequence of digits, nothing else) with a \n in the end  */
void printInteger(Integer a) {
	node *ptr = a.head;
	int flag = 1;
	if(ptr) {
		do {
			if(flag) {
				flag = (ptr->val == '0'); //keep eating 0 till first non zero is encountered
				if(!flag)
					ptr = ptr->prev;
				ptr = ptr->next;
			}
			else {
				printf("%d",ptr->val);
				ptr = ptr->next;
			}
		} while(ptr->next);
		printf("%d",ptr->val);
	}
	else
		printf("0");
	putchar('\n');
}


/* Destroys an Integer, i.e. deallocates all malloced memory
 * for the Integer and reInitiatizes it */
void destroyInteger(Integer *a);




int main() {
	Integer a, b, c;
	char ch;
	char str[MAX]; /* This size may change */
	initInteger(&a);
	initInteger(&b);
	initInteger(&c);
	while((ch = getchar()) != '\n')
		addDigit(&a, ch);
	scanf("%s", str);
	b = createIntegerFromString(str);
	c = addIntegers(a, b);
	printInteger(c);
	//destoryInteger(&c);
	c = substractIntegers(a, b);
	printInteger(c);
	//destoyInteger(&c);
	return 0;
}
