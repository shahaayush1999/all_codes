typedef struct node {
	int num;
	struct node* next;
} node;
typedef node* stack;
void init(stack *s);
int isempty(stack *s);
int isfull(stack *s);
void push(stack *s, int num);
int pop(stack *s);
