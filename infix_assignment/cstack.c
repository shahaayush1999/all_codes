#include <stdlib.h>
#include "cstack.h"
void cinit(cstack *s) {
	*s = NULL;
}

int cisempty(cstack *s) {
	return *s == NULL;
}

int cisfull(cstack *s) {
	return 0;
}

void cpush(cstack *s, char num) {
	cnode *temp = (cnode *) malloc(sizeof(cnode));
	cnode *tempnode = *s;
	temp->num = num;
	temp->next = NULL;
	if(!*s)
		*s = temp;
	else {
		while(tempnode->next)
			tempnode = tempnode->next;
		tempnode->next = temp;
	}
}

char cpop(cstack *s) {
	char ret;
	cnode *temp = *s;
	if((*s)->next == NULL) {
		ret = (*s)->num;
		free(*s);
		*s = NULL;
	}
	else {
		while(temp->next->next)
			temp = temp->next;
		ret = temp->next->num;
		free(temp->next);
		temp->next = NULL;
	}
	return ret;
}
