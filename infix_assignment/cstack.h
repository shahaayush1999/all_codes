typedef struct cnode {
	char num;
	struct cnode* next;
} cnode;
typedef cnode* cstack;
void cinit(cstack *s);
int cisempty(cstack *s);
int cisfull(cstack *s);
void cpush(cstack *s, char num);
char cpop(cstack *s);
