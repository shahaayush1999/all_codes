#include "cstack.h"
#include "stack.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#define MAX 64
//#defines for token type
#define OPERAND	100
#define OPERATOR 200
#define END 300
#define ERROR 400

typedef struct token {
	int type; // OPERAND, OPERATOR, END, ERROR, OPENBR, CLOSEDBR
	int val; // will be used if type==OPERAND
	char op; // will e used if type == OPERATOR
}token;

enum states { NUM, OPR, FINISH, ERR, SPC};

int readline(char *line, int len) {
	int i;
	char ch;
	i = 0;
	while(i < len - 1) {
		ch = getchar();
		if(ch == '\n') {
			line[i++] = '\0';
			return i - 1;
		}
		else
			line[i++] = ch;
	}
	line[len - 1] = '\0';
	return len - 1;
}

int add(int x, int y) {
	return (x + y);
}
int subtract(int x, int y) {
	return (x - y);
}
int multiply(int x, int y) {
	return (x * y);
}
int divide(int x, int y) {
	return (x / y);
}
int modulo(int x, int y) {
	return (x % y);
}
/*
int pow(int x, int y) {

}
*/

int operation(char op, int x, int y) {
	switch(op) {
		case '+':
			return add(x, y);
			break;
		case '-':
			return subtract(x, y);
			break;
		case '*':
			return multiply(x, y);
			break;
		case '/':
			return divide(x, y);
			break;
		case '%':
			return modulo(x, y);
			break;
		//case '^':
			//return pow(x, y);
		default:
			return INT_MIN;
	}
	return INT_MIN;
}

token gettoken(char *expr, int *reset) {
	token t;
	static int no;
	static int i = 0;
	static enum states currstate = SPC;
	char currchar;
	if(*reset == 1) {
		*reset = 0;
		i = 0;
		currstate = SPC;
	}
	while(1) {
		currchar = expr[i];
		// depending on currstate, and currchar, change state, and take action.
		switch(currstate) {
			case NUM:
				switch(currchar) {
					case '0': case '1': case '2': case '3':
					case '4': case '5': case '6': case '7':
					case '8': case '9':
						no = no * 10 + currchar - '0';
						currstate = NUM;
						i++;
						break;
					case '+': case '-': case '*': case '/': case '%': case '(': case ')':
						t.type = OPERAND;
						t.val = no;
						no = 0;
						currstate = OPR;
						i++;
						return t;
						break;
					case ' ':
						t.type = OPERAND;
						t.val = no;
						no = 0;
						currstate = SPC;
						i++;
						return t;
						break;
					case '\0':
						t.type = OPERAND;
						t.val = no;
						no = 0;
						currstate = FINISH;
						i++;
						return t;
						break;
					default:
						t.type = OPERAND;
						t.val = no;
						no = 0;
						currstate = ERR;
						i++;
						return t;
						break;
				}
				break;
			case OPR:
				t.type = OPERATOR;
				t.op = expr[i - 1];
				i++;
				switch(currchar) {
					case '0': case '1': case '2': case '3':
					case '4': case '5': case '6': case '7':
					case '8': case '9':
						no = currchar - '0';
						currstate = NUM;
						break;
					case '+': case '-': case '*': case '/': case '%': case '(': case ')':
						currstate = OPR;
						break;
					case ' ':
						currstate = SPC;
						break;
					case '\0':
						currstate = FINISH;
						break;
					default:
						currstate = ERR;
						break;
				}
				return t;
				break;
			case SPC:
				switch(currchar) {
					case '0': case '1': case '2': case '3':
					case '4': case '5': case '6': case '7':
					case '8': case '9':
						no = currchar - '0';
						currstate = NUM;
						break;
					case '+': case '-': case '*': case '/': case '%': case '(': case ')':
						currstate = OPR;
						break;
					case ' ':
						currstate = SPC;
						break;
					case '\0':
						currstate = FINISH;
						break;
					default:
						currstate = ERR;
						break;
				}
				i++;
				break;
			case FINISH:
				t.type = END;
				return t;
				break;
			case ERR:
				t.type = ERROR;
				return t;
				break;
		}
	}
	return t;
}

int precedence(char op) {
	switch(op) {
		case '+': case '-':
			return 1;
			break;
		case '*': case '/': case '%':
			return 2;
			break;
		case ')':
			return 4;
		case '(':
			return 5;
		default:
			//printf("ERR:Asked for precedence of %c\n", op);
			return 0;
			break;
	}
	return 6;
}

char cpeek(cstack *cs) {
	char c = '\0';
	if(!cisempty(cs)) {
		c = cpop(cs);
		cpush(cs, c);
	}
	return c;
}

int infixeval(char *infix) {
	token t;
	cstack cs;
	stack s;
	int reset = 1, result = 0;
	int x = 0, y = 0, z = 0;
	char incomingop;
	char prevop;
	init(&s);
	cinit(&cs);
	while(1) {
		t = gettoken(infix, &reset);
		//printf("Token: Type = %d, Val = %d, Op = %c\n", t.type, t.val, t.op);
		if(t.type == OPERAND) {
			if(!isfull(&s)) {
				//printf("1STACK IN: %d\n", t.val);
				push(&s, t.val);
			}
		}
		else if(t.type == OPERATOR) {
				incomingop = t.op;
				if(incomingop == '(') {
					//printf("2CSTACK IN: %c\n", incomingop);
					cpush(&cs, incomingop);
				}
				else if(incomingop == ')') {
					do {
						if(!cisempty(&cs)) {
							prevop = cpop(&cs);
							//printf("3CSTACK OUT: %c\n", prevop);
						}
						
						else if (cisempty(&cs)){
							//fprintf(stderr, "1Insufficient operators or unmatched parenthesis\n");
							return INT_MIN;
						}
						if(prevop == '(')
							break;
						else {
							if(!isempty(&s)) {
								y = pop(&s);
								//printf("4STACK OUT: %d\n", y);
							}
							if(!isempty(&s)) {
								x = pop(&s);
								//printf("5STACK OUT: %d\n", x);
							}
							else {
								//fprintf(stderr, "2Insufficient operands\n");
								return INT_MIN;
							}
							//printf("1 operating %d %c %d\n", x, prevop, y);
							z = operation(prevop, x, y);
							//printf("6STACK IN: %d\n", z);
							push(&s, z);
						}
					} while(prevop != '(');
				}
				else if(cisempty(&cs)) {
					//printf("7CSTACK IN: %c\n", incomingop);
					cpush(&cs, incomingop);
				}
				else if(!cisempty(&cs) && (prevop = cpeek(&cs)) && (precedence(prevop) < precedence(incomingop))) {
					//printf("8CSTACK IN: %c\n", incomingop);
					cpush(&cs, incomingop);
				}
				else if(!cisempty(&cs) && (prevop = cpeek(&cs)) && (prevop == '(')) {
					//printf("8CSTACK IN: %c\n", incomingop);
					cpush(&cs, incomingop);
				}
				else {
					if(!cisempty(&cs)) {
						prevop = cpop(&cs);
						//printf("9CSTACK OUT: %c\n", prevop);
					}
					if(prevop == '(') {
						;
					}
					else if((precedence(prevop) >= precedence(incomingop))) {
						while((precedence(prevop) >= precedence(incomingop)) /*tb*/ && (precedence(prevop) < 5)/*tb*/) {
							if(!isempty(&s)) {
								y = pop(&s);
								//printf("10STACK OUT: %d\n", y);
							}
							if(!isempty(&s)) {
								x = pop(&s);
								//printf("11STACK OUT: %d\n", x);
							}
							else {
								//fprintf(stderr, "3insufficient operands\n");
								return INT_MIN;
							}
							
							//printf("2 operating %d %c %d\n", x, prevop, y);
							z = operation(prevop, x, y);
							if(!isfull(&s)) {
								//printf("12STACK IN: %d\n", z);
								push(&s, z);
							}
							if((!cisempty(&cs)) && (prevop = cpeek(&cs)) && (precedence(prevop) >= precedence(incomingop))/*tb*/ && (precedence(prevop) < 5)/*tb*/) {
								prevop = cpop(&cs);
								//printf("13CSTACK OUT: %c\n", prevop);
							}
							else {
								break;
							}
						}
					}
					else {
						//printf("14CSTACK IN: %c\n", incomingop);
						cpush(&cs, incomingop);
					}
					
					if(!cisfull(&cs)) {
						//printf("15CSTACK IN: %c\n", incomingop);
						cpush(&cs, incomingop);
					}
				}
		}
		else if(t.type == END) {
			while(!cisempty(&cs)) {
				prevop = cpop(&cs);
				//printf("16CSTACK OUT: %c\n", prevop);
				if(!isempty(&s)) {
					y = pop(&s);
				}
				if(!isempty(&s)) {
					x = pop(&s);
				}
				else {
					//fprintf(stderr, "7insufficient operands\n");
					return INT_MIN;
				}
				//printf("3 operating %d %c %d\n", x, prevop, y);
				z = operation(prevop, x, y);
				if(!isfull(&s)) {
					//printf("17STACK IN: %d\n", z);
					push(&s, z);
				}
			}
			if(!isempty(&s))
				result = pop(&s);
			else {
				//fprintf(stderr, "4Insufficient operands or malformed expression\n");
				return INT_MIN;
			}
			break;
		}
		else if(t.type == ERROR) {
				//fprintf(stderr, "5Invalid character or malformed expression\n");
				return INT_MIN;
		}
	}
	return result;
}

int main(int argc, char *argv[]) {
	char line[MAX];
	int x = 0;
	while(readline(line, MAX)) {
		x = infixeval(line);
		if(x == INT_MIN) {
			fprintf(stderr, "Bad infix expression\n");
			continue;
		}
		else
			printf("%d\n", x);
	}
	return 0;
}
