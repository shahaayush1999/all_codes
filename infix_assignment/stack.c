#include <stdlib.h>
typedef struct node {
	int num;
	struct node* next;
} node;
typedef node* stack;
void init(stack *s) {
	*s = NULL;
}

int isempty(stack *s) {
	return *s == NULL;
}

int isfull(stack *s) {
	return 0;
}

void push(stack *s, int num) {
	node *temp = (node *) malloc(sizeof(node));
	node *tempnode = *s;
	temp->num = num;
	temp->next = NULL;
	if(!*s)
		*s = temp;
	else {
		while(tempnode->next)
			tempnode = tempnode->next;
		tempnode->next = temp;
	}
}

int pop(stack *s) {
	int ret;
	node *temp = *s;
	if((*s)->next == NULL) {
		ret = (*s)->num;
		free(*s);
		*s = NULL;
	}
	else {
		while(temp->next->next)
			temp = temp->next;
		ret = temp->next->num;
		free(temp->next);
		temp->next = NULL;
	}
	return ret;
}










