#include <stdio.h>
#define MAX 128

typedef struct stack {
	int *array;
	int i;
} stack;

void init(stack *s) {
	s->array = (int *) malloc(sizeof(int) * MAX);
	s->i = 0;
}

void push(stack *s, int x) {
	s->array[s->i++] = x;
}

int pop(stack *s) {
	return = s->array[s->--i];
}

int isempty(stack *s) {
	return s->i == 0;
}
int isfull(stack *s) {
	return s->i == MAX
}

int main() {
	stack s;
	init(&s);

	int x;

	while(scanf("%d ", &x) != -1)
		push(&s, x);

	while(!isempty(&s))
		printf("%d\n", pop(&s));

	return 0;
}
