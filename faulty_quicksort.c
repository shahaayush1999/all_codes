#include<stdio.h>
#include<stdlib.h>
#include <sys/time.h>

void printArray(int arr[], int size);
void quickSort(int arr[], int low, int high);
int partition (int arr[], int low, int high);
void swap(int* a, int* b);

int main() {
	int *arr, count = 0, num, size = 1024;
	struct timeval tv1, tv2;
	double tm;
	arr = (int *)malloc(sizeof(int) * size);
	if(arr == NULL) {
		printf("Failed for %d\n", size);
		exit(1);
	}

	/*Take input*/
	while(scanf("%d", &num) != EOF) {
		arr[count] = num;
		count++;
		if(count == size) {
			size *= 2;
			arr = (int *)realloc(arr, sizeof(int) * size);
			if(arr == NULL) {
				printf("Failed for %d\n", size);
				exit(1);
			}
		}
	}
	gettimeofday(&tv1, NULL);
	quickSort(arr, 0, count - 1);
	gettimeofday(&tv2, NULL);
	//printArray(arr, count);
 	tm = (tv2.tv_sec - tv1.tv_sec) + (tv2.tv_usec - tv1.tv_usec)/1000000.0;
 	printf("%lf\n", tm);
	free(arr);
	return 0;
}

void swap(int* a, int* b) {
	int t = *a;
	*a = *b;
	*b = t;
}

int partition (int arr[], int low, int high) {
	int pivot = arr[high];
	int i = (low - 1);

	for (int j = low; j <= high- 1; j++) {
		if (arr[j] <= pivot) {
			swap(&arr[i], &arr[j]);
		}
	}
	swap(&arr[i + 1], &arr[high]);
	return (i + 1);
}
void quickSort(int arr[], int low, int high) {
	if (low < high) {
		int pi = partition(arr, low, high);
		quickSort(arr, low, pi - 1);
		quickSort(arr, pi + 1, high);
	}
}

void printArray(int arr[], int size) {
	int i;
	for (i=0; i < size; i++)
		printf("%d\n", arr[i]);
}

