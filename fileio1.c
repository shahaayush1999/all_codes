#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

int main(int argc, char *argv[]) {
	FILE *fp;
	int i = 20;
	char str[4]="20";
	char intstr[4];
	sprintf(intstr,"%d", i);
	printf("write(1, &i, sizeof(i))\n");
	write(1, &i, sizeof(i));
	printf("\nwrite(1, str, 2)\n");
	write(1, str, 2);
	printf("\nwrite(1, intstr, strlen(intstr))\n");
	write(1, intstr, strlen(intstr));
	return 0;
}
