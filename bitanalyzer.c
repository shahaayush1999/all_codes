#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main (int argc, char *argv[]) {
	char filename[30];
	if(argc ==2)
		strcpy(filename, argv[1]);
	else {
		printf("File to open: ");
		scanf("%s", filename);
	}
	
	FILE *f1 = fopen(filename, "r");
	if(f1 == NULL) {
		perror("ERROR");
		return errno;
	}
	int a = 0, count = 0;
	double b = 0;
	char c, high;
	printf("# is space, return is $, tab is %%\n");
	printf("CHAR:\n\n");
	while(fread(&c, 1, 1, f1) == 1) {
		if(c == ' ')
			c = '#';
		if(c == '\n')
			c = '$';
		if(c == '\t')
			c = '%';
		if(c == 46 || c == '%' || c == '#' || c == '$' || c == 32 || (c > 47 && c < 58) || (c > 64 && c < 91) || (c > 96 && c < 123))
			high = '*';
		else
			high = ' ';
		/*
		printf("CHAR %3d = %c %2c\n", count, high, c);
		*/
		if(high  == '*')
			printf("CHAR %3d = %c %6c\n", count, high, c);
		else if(high  == ' ')
			printf("CHAR %3d = %3d\n", count, (unsigned int)c);
		++count;
	}
	/*
	count = 0;
	fseek(f1, 273, SEEK_SET);
	printf("\n\n\nINT:\n\n");
	while(fread(&a, 4, 1, f1) == 1) {
		printf("INT %2d = %d\n", (count / 4), a);
		count += 4;
	}
	*/
	/*
	count = 0;
	fseek(f1, 0, SEEK_SET);
	printf("\n\nDOUBLE:\n\n");
	while(fread(&b, 8, 1, f1) == 1) {
		printf("DOUBLE %2d = %.0lf\n", count, b);
		count++;
	}
	*/
	fclose(f1);
	return 0;
}
