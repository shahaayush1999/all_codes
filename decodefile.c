#include <stdio.h>
#include <string.h>
#include <errno.h>

typedef struct structure {
	int a;
	int b;
	char c[40];
} structure;

int main(int argc, char *argv[]) {
	char filename[30];
	
	 structure apple;

	if(argc == 2)
		strcpy(filename, argv[1]);
	else {
		printf("Enter filename: ");
		scanf("%s", filename);
	}
	printf("Sizeof struct  = %d\n", (int)sizeof(structure));
	FILE *fp = fopen(filename, "r");
	if(fp == NULL) {
		perror("ERROR");
		return errno;
	}
	while(fread(&apple ,sizeof(structure) , 1, fp) == 1) {
		printf("%d\n%s\n", apple.a, apple.c);
	}
	fclose(fp);
	return 0;
}
