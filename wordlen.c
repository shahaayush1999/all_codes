#include <stdio.h>
#include <stdlib.h>

int main() {
	int c = 0, lettercount = 0, highest = 0, x = 0, y = 0, *wordlength = (int *)calloc(50, sizeof(int));
	while ((c = getchar()) != EOF) {
		if(c == ' ' || c == '\n' || c == '\t' ) {
			if (lettercount > 0) {
				++wordlength[lettercount];
				if(wordlength[lettercount] > highest)
					highest = wordlength[lettercount];
				lettercount = 0;
			}
		}
		else {
			++lettercount;
		}
	}



	printf("highest: %d", highest);
	for (x = 0; x < 50; x++) {
		printf("%d", x);
		for(y = 0; y < (((float)wordlength[x] * 9000000)/ highest); y++)
			printf("|");		
		printf("\n");
	}
	return 0;
}
