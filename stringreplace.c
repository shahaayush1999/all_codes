#include <stdio.h>
#include <string.h>

int stringreplace(char *text, char *orig, char *new);

int main() {
    char text[128], orig[128], new[128]; // don't assume size of 128 in the function stringreplace, it is not available in the function!
    int count;
    while(scanf("%s%s%s", text, orig, new) != -1) {
       count = stringreplace(text, orig, new);
       printf("%d %s\n", count, text);
    }
}

int stringreplace(char *text, char *orig, char *new) {
	int count, x = 0, y = 0, temp = 0, c = 0, lentext = strlen(text), lenorig = strlen(orig), lennew = strlen(new);
	char newstr[128];
	while(1) {
		if(x >= lentext)
			break;
			
		else if(orig[0] != text[x]) {
			newstr[c] = text[x];
			c++;
			x++;
		}
		
		else if(orig[0] == text[x]) {
			for(temp = 0; temp < lenorig; temp++) {
				if(text[temp + x] != orig[temp])
					break;
			}
			if(temp == lenorig) {
				for(y = 0; y < lennew; y++) {
					newstr[c] = new[y];
					c++;
					x++;
				}
				x--;
				count++;
			}
			else {
				newstr[c] = text[x];
				c++;
				x++;
			}
		}
	}
	newstr[c] = '\0';
	strcpy(text, newstr);
	return count;
}
